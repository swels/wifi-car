load('api_gpio.js');
load('api_pwm.js');
load('api_timer.js');
load('api_rpc.js');
load('api_log.js');
load('api_math.js');

GPIO.set_mode(4, GPIO.MODE_OUTPUT); //D2
GPIO.write(4, 0);

GPIO.set_mode(5, GPIO.MODE_OUTPUT); //D1
GPIO.write(5, 0);

GPIO.set_mode(12, GPIO.MODE_OUTPUT); //D6
GPIO.write(12, 0);

GPIO.set_mode(13, GPIO.MODE_OUTPUT); //D7
GPIO.write(13, 0);

GPIO.set_mode(14, GPIO.MODE_OUTPUT); //D5
GPIO.write(14, 0);

GPIO.set_mode(15, GPIO.MODE_OUTPUT); //D8
GPIO.write(15, 0);

let wakeTimeoutId = 0;
let mainLoopFreq = 10; //ms
let mainLoopIntervalId = 0;
let mainProcessTimeoutId = 0;

let wake = false;
let wakeTime = 1000; //ms

let motor = (function() {

  let motor = {};

  let init = function() {
    
    motor.power = {
      left : 0,
      right : 0
    };

    motor.pin = {
      left: 4, //D2
      leftPositive: 14, //D5
      leftNegative: 12, //D6
      right: 5, //D1
      rightPositive: 13, //D7
      rightNegative: 15, //D8
    };
    stop();
  };

  let update = function() {
    //Log.info(JSON.stringify(motor.power.left));
    PWM.set(motor.pin.left, 1000, Math.abs(motor.power.left));
    PWM.set(motor.pin.right, 1000, Math.abs(motor.power.right));

    if(motor.power.left > 0) {
      GPIO.write(motor.pin.leftPositive, 1);
      GPIO.write(motor.pin.leftNegative, 0);
    } else {
      GPIO.write(motor.pin.leftPositive, 0);
      GPIO.write(motor.pin.leftNegative, 1);
    }

    if(motor.power.right > 0) {
      GPIO.write(motor.pin.rightPositive, 1);
      GPIO.write(motor.pin.rightNegative, 0);
    } else {
      GPIO.write(motor.pin.rightPositive, 0);
      GPIO.write(motor.pin.rightNegative, 1);
    }
  };

  let stop = function() {
    GPIO.write(motor.pin.left, 0);
    GPIO.write(motor.pin.leftPositive, 0);
    GPIO.write(motor.pin.leftNegative, 0);
    GPIO.write(motor.pin.right, 0);
    GPIO.write(motor.pin.rightPositive, 0);
    GPIO.write(motor.pin.rightNegative, 0);
  };

  let setPower = function(_power) {
    if(_power.left && Math.abs(_power.left) <= 100) motor.power.left = _power.left;
    if(_power.right && Math.abs(_power.right) <= 100) motor.power.right = _power.right;
  };

  init();

  // export
  motor.init = init;
  motor.stop = stop;
  motor.update = update;
  motor.setPower = setPower;

  return motor;
})();

let mainLoop = function() {

  // to prevent stacking blocked main process
  Timer.del(mainProcessTimeoutId);

  if(!wake) {
    Timer.del(mainLoopIntervalId);
    mainLoopIntervalId = 0;
    motor.stop();
    return;
  }
  
  // queue one singel stack
  mainProcessTimeoutId = Timer.set(0, false, function() {
    motor.update();
  }, null);
  
};


RPC.addHandler('Wake', function(args) {
  
  wake = true;
  Timer.del(wakeTimeoutId);

  wakeTimeoutId = Timer.set(wakeTime, false, function() {
    wake = false;
  }, null);
  
  if(!mainLoopIntervalId) {
    mainLoopIntervalId = Timer.set(mainLoopFreq, true, mainLoop, null);
  }
  
});

RPC.addHandler('Motor', function(args) {
  motor.setPower(args);
});


/*
let dir = -1;
let power = 100;
Timer.set(10, true, function() {
  PWM.set(15, 1000, power);
  power += 1 * dir;
  
  if(power <= 0 || power >= 100) {
    dir *= -1;
  }
  
}, null);*/

/*
GPIO.set_mode(15, GPIO.MODE_OUTPUT);
GPIO.write(15, 1);
Timer.set(500, true, function() {
  GPIO.toggle(15);
}, null);*/